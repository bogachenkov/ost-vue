import Vue from 'vue';
import Router from 'vue-router';

import store from '../store';

import LoginPage from '../views/Auth/LoginPage.vue';
import RegisterPage from '../views/Auth/RegisterPage.vue';
import DatasetsPage from '../views/DatasetsPage.vue';
import MarkupPage from '../views/MarkupPage.vue';
import DashboardPage from '../views/Dashboard/DashboardPage.vue';
import UsersPage from '../views/Dashboard/UsersPage.vue';
import UsersAddPage from '../views/Dashboard/UsersAddPage.vue';
import DashboardDatasetsPage from '../views/Dashboard/DatasetsPage.vue';
import DatasetsAddPage from '../views/Dashboard/DatasetsAddPage.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/datasets'
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage,
      meta: {
        onlyGuest: true
       }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterPage,
      meta: {
        onlyGuest: true
       }
    },
    {
      path: '/datasets',
      name: 'datasets',
      component: DatasetsPage,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/datasets/:title',
      component: MarkupPage,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/dashboard',
      component: DashboardPage,
      redirect: '/dashboard/users',
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: 'users',
          component: UsersPage,
        },
        {
          path: 'users/add',
          component: UsersAddPage
        },
        {
          path: 'datasets',
          component: DashboardDatasetsPage,
        },
        {
          path: 'datasets/add',
          component: DatasetsAddPage
        }
      ]
    }
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const onlyGuest = to.matched.some(record => record.meta.onlyGuest);
  if (requiresAuth && !store.state.auth.isAuth && !store.state.token.checkingToken) {
     return next('/login');
  }
  if (onlyGuest && store.state.auth.isAuth && !store.state.token.checkingToken) {
    return next('/')
  }
  return next();
})

export default router;
