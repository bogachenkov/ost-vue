import Vue from 'vue';
import { SET_ERROR, INCREMENT_REQUESTS, DECREMENT_REQUESTS } from './types';

export default {
  [SET_ERROR] (state, payload) {
    state.error = payload.message;
    Vue.notify({
      group: 'err',
      title: 'Important message',
      text: 'Hello user! This is a notification!'
    })
  },
  [INCREMENT_REQUESTS] (state) {
    state.error = '';
    state.requests += 1;
  },
  [DECREMENT_REQUESTS] (state) {
    state.error = '';
    state.requests -= 1;
  }
}
