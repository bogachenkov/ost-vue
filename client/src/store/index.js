import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import token from './modules/token';
import register from './modules/register';
import datasets from './modules/datasets';
import markups from './modules/markups';
import users from './modules/users';

import rootMutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    requests: 0,
    error: ''
  },
  mutations: {
    ...rootMutations
  },
  modules: {
    auth,
    token,
    register,
    datasets,
    users,
    markups
  }
})

export default store;
