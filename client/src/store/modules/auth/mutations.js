import { LOGIN_SUCCESS, LOGIN_FAILS, LOG_OUT } from '../../types';

export default {
  [LOGIN_SUCCESS] (state) {
    state.isAuth = true;
  },
  [LOGIN_FAILS] (state) {
    state.isAuth = false;
  },
  [LOG_OUT] (state) {
    state.isAuth = false;
  }
}
