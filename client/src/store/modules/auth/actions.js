import axios from 'axios';
import router from '../../../router/index';
import { LOGIN_SUCCESS, LOG_OUT, INCREMENT_REQUESTS, DECREMENT_REQUESTS, SET_ERROR } from '../../types';

export default {
  login ({ commit, state }, credentials) {
    commit(INCREMENT_REQUESTS);
    axios.post('/auth/login', credentials)
      .then(response => {
        const {token, user} = response.data;
        //Save token to localStorage and set it to headers
        localStorage.setItem('token', token);
        axios.defaults.headers.common['Authorization'] = token;

        commit(DECREMENT_REQUESTS);
        commit(LOGIN_SUCCESS, user);
        router.push('/');
      })
      .catch(err => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, err.response.data)
      })
  },

  logout ({ commit }) {
    localStorage.removeItem('token');
    axios.defaults.headers.common['Authorization'] = null;
    commit(LOG_OUT);
    router.push('/login')
  }
}
