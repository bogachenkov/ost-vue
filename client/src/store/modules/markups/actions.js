import axios from 'axios';
import { INCREMENT_REQUESTS, DECREMENT_REQUESTS, SET_ERROR, FETCH_MARKUPS_SUCCESS, SAVE_MARKUP_SUCCESS } from '../../types';

export default {
  fetchMarkups ({ commit }, folder)  {
    commit(INCREMENT_REQUESTS);
    axios.get(`/markups/${folder}`)
      .then(response => {
        commit(DECREMENT_REQUESTS);
        commit(FETCH_MARKUPS_SUCCESS, response.data);
      })
      .catch(error => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, error.response.data);
      })
  },
  saveMarkup ({ commit }, credentials) {
    commit(INCREMENT_REQUESTS);
    axios.post('/markups/add', credentials)
      .then(response => {
        console.log(response.data.name);
        commit(DECREMENT_REQUESTS);
        commit(SAVE_MARKUP_SUCCESS, response.data.name);
      })
      .catch(error => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, error.response.data);
      })
    }
}
