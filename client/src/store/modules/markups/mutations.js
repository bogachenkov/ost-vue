import { FETCH_MARKUPS_SUCCESS, SAVE_MARKUP_SUCCESS } from '../../types';

export default {
  [FETCH_MARKUPS_SUCCESS] (state, payload) {
    state.list = shuffle(payload);
  },
  [SAVE_MARKUP_SUCCESS]( state, payload ) {
    state.list = removeMarkedImage(state, payload);
  }
}

const removeMarkedImage = (state, name) => {
  const index = state.list.indexOf(name);
  if (index !== -1) state.list.splice(index, 1);
  return state.list;
}

const shuffle = arr => {
  const shuffleArray = [...arr];
  for (let i = shuffleArray.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [shuffleArray[i], shuffleArray[j]] = [shuffleArray[j], shuffleArray[i]];
  }
  return shuffleArray;
}
