import axios from 'axios';
import router from '../../../router';
import {
  INCREMENT_REQUESTS,
  FETCH_USERS_SUCCESS,
  DECREMENT_REQUESTS,
  SET_ERROR,
  BLOCK_USER_SUCCESS
} from '../../types';

export default {
  fetchUsers ({ commit }) {
    commit(INCREMENT_REQUESTS);
    axios.get('/users')
      .then(response => {
        commit(DECREMENT_REQUESTS);
        commit(FETCH_USERS_SUCCESS, response.data);
      })
      .catch(error => {
        commit(SET_ERROR, error.response.data)
      })
  },
  addUser ({ commit }, credentials) {
    commit(INCREMENT_REQUESTS);
    axios.post('/users/add', credentials)
      .then(res => {
        commit(DECREMENT_REQUESTS);
        router.push('/dashboard/users')
      })
      .catch(error => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, error.response.data)
      })
  },
  blockUser ({ commit }, credentials) {
    commit(INCREMENT_REQUESTS);
    axios.put(`/users/${credentials}/block`)
    .then(response => {
      commit(DECREMENT_REQUESTS);
      commit(BLOCK_USER_SUCCESS, response.data);
    })
    .catch(error => {
      console.log(error);
      commit(DECREMENT_REQUESTS);
      commit(SET_ERROR, error.response.data);
    })
  }
}
