import { FETCH_USERS_SUCCESS, BLOCK_USER_SUCCESS } from '../../types';

export default {
  [FETCH_USERS_SUCCESS] (state, payload) {
    state.list = payload;
  },
  [BLOCK_USER_SUCCESS] (state, payload) {
    state.list = state.list.map(user => {
      if (user._id === payload._id) return { ...payload };
      return user;
    })
  }
}
