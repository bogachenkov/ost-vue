import axios from 'axios';
import router from '../../../router';
import { CHECK_TOKEN_FAILS, CHECK_TOKEN_SUCCESS, CHECK_TOKEN_PENDING, LOG_OUT, LOGIN_SUCCESS } from '../../types';

export default {
  checkTokenOnMount ({ commit, state, dispatch }) {
    const token = localStorage.getItem('token');
    // If no token found, user should sign in
    if (!token) {
      dispatch('logout')
      commit(CHECK_TOKEN_FAILS );
    } else {
      // Else get user data by token
      axios.defaults.headers.common['Authorization'] = token;

      commit(CHECK_TOKEN_PENDING);

      axios.get(`/auth/user`)
        .then(response => {
          commit(CHECK_TOKEN_SUCCESS);
          commit(LOGIN_SUCCESS)
        })
        .catch(error => {
          // If token invalid or expired remove it and logout user
          if (error.response && error.response.status === 401) {
            commit(CHECK_TOKEN_FAILS );
            dispatch('logout');
          }
        })
    }
  }
}
