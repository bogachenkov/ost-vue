import { CHECK_TOKEN_PENDING, CHECK_TOKEN_SUCCESS, CHECK_TOKEN_FAILS } from '../../types';

export default {
  [CHECK_TOKEN_PENDING] (state) {
    state.checkingToken = true;
  },
  [CHECK_TOKEN_SUCCESS] (state) {
    state.checkingToken = false;
  },
  [CHECK_TOKEN_FAILS] (state) {
    state.checkingToken = false;
  },
}
