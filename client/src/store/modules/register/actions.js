import axios from 'axios';
import router from '../../../router';

import { INCREMENT_REQUESTS, DECREMENT_REQUESTS, SET_ERROR, LOGIN_SUCCESS } from '../../types';

export default {
  register ({ commit, state }, credentials) {
    commit(INCREMENT_REQUESTS);
    axios.post('/auth/register', credentials)
      .then(response => {
        const { token, user } = response.data;

        //Save token to localStorage and set it to headers
        localStorage.setItem('token', token);
        axios.defaults.headers.common['Authorization'] = token;

        if (token) {
          commit(DECREMENT_REQUESTS);
          commit(LOGIN_SUCCESS)
          router.push('/');
        }
      })
      .catch(error => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, error.response.data);
      })
  }
}
