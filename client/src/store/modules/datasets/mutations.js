import { FETCH_DATASETS_COMPLETE } from '../../types';

export default {
  [FETCH_DATASETS_COMPLETE] (state, payload) {
    state.list = payload;
  }
}
