import axios from 'axios';
import router from '../../../router';

import { INCREMENT_REQUESTS, DECREMENT_REQUESTS, SET_ERROR, FETCH_DATASETS_COMPLETE } from '../../types';

export default {
  addDataset ({ commit, state }, credentials) {
    commit(INCREMENT_REQUESTS);
    axios.post('/datasets/add', credentials)
      .then(response => {
        router.push("/dashboard/datasets");
        commit(DECREMENT_REQUESTS);
      })
      .catch(error => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, error.response.data);
      })
  },

  fetchDatasets ({ commit, state }) {
    commit(INCREMENT_REQUESTS);
    axios.get('/datasets')
      .then(response => {
        commit(DECREMENT_REQUESTS);
        commit(FETCH_DATASETS_COMPLETE, response.data);
      })
      .catch(error => {
        commit(DECREMENT_REQUESTS);
        commit(SET_ERROR, error.response.data);
      })
  }
}
