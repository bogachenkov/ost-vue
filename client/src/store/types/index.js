export const INCREMENT_REQUESTS = "INCREMENT_REQUESTS";
export const DECREMENT_REQUESTS = "DECREMENT_REQUESTS";
export const SET_ERROR = "SET_ERROR";

export const CHECK_TOKEN_PENDING = "CHECK_TOKEN_PENDING";
export const CHECK_TOKEN_SUCCESS = "CHECK_TOKEN_SUCCESS";
export const CHECK_TOKEN_FAILS = "CHECK_TOKEN_FAILS";

export const LOGIN_PENDING = "LOGIN_PENDING";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILS = "LOGIN_FAILS";

export const LOG_OUT = "LOG_OUT";

export const ADD_USER_COMPLETED = 'ADD_USER_COMPLETED';

export const BLOCK_USER_SUCCESS = "BLOCK_USER_SUCCESS";

export const FETCH_DATASETS_COMPLETE = "FETCH_DATASETS_COMPLETE";

export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";

export const FETCH_MARKUPS_SUCCESS = "FETCH_MARKUPS_SUCCESS";
export const SAVE_MARKUP_SUCCESS = "SAVE_MARKUP_SUCCESS";
