import Vue from 'vue';
import Notifications from 'vue-notification';

import App from './App.vue';

import router from './router';
import store from './store';

// Import components
import Loader from './components/Loader.vue';
import Container from './components/Container.vue';
import Button from './components/Button.vue';
import Redirect from './components/Redirect.vue';

Vue.component('loader', Loader);
Vue.component('container', Container);
Vue.component('redirect', Redirect);
Vue.component('custom-button', Button);

Vue.use(Notifications);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
