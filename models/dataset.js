const mongoose = require('mongoose');

const datasetSchema = new mongoose.Schema({
  title: String,
  folder: String
});

const Dataset = mongoose.model('Dataset', datasetSchema);

module.exports = Dataset;
