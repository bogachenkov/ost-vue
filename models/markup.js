const mongoose = require('mongoose');

const markupSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  image: String,
  dataset: String,
  coords: [Object]
});

const Markup = mongoose.model('Markup', markupSchema);

module.exports = Markup;
