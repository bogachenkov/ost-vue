const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  login:  {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  blocked: {
    type: Boolean,
    default: false
  },
  imagesMarked: {
    type: Number,
    default: 0
  }
},
{
  timestamps: true
});

const User = mongoose.model('User', userSchema);

module.exports = User;
