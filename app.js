require('dotenv').config();
const Koa = require('koa');
const Router = require('koa-router');
const BodyParser = require('koa-bodyparser');
const logger = require('koa-logger');
const mongoose = require('mongoose');
const passport = require('koa-passport');
const mount = require('koa-mount');
const serve = require('koa-static');
const cors = require('@koa/cors');
const helmet = require("koa-helmet");
const respond = require('koa-respond');

const app = new Koa();
const router = new Router();

// Logger
if (process.env.NODE_ENV = 'development') app.use(logger());

// Body parser
const bodyParcerOpts = {
  enableTypes: ['json'],
  jsonLimit: '5mb',
  strict: true,
  onerror: function (err, ctx) {
    ctx.throw('Body parse error', 422)
  }
}
app.use(BodyParser());

// Respond
app.use(respond());

// Helmet
app.use(helmet());

//Error handler
app.use(async function handleError(context, next) {
  try {
    await next();
  } catch (error) {
    context.status = 500;
    context.body = error;
  }
});

// Passport
app.use(passport.initialize());
require('./passport')(passport);

// Cors
app.use(cors());

// Database
const dbuser = process.env.MONGO_USER;
const dbpassword = process.env.MONGO_PASSWORD;

// Static files
//app.use('/datasets', express.static(__dirname + '/datasets'))
app.use(mount('/datasets', serve(__dirname + '/datasets')));

mongoose.Promise = Promise;
mongoose.set('useNewUrlParser', true);
mongoose.connect(`mongodb://${dbuser}:${dbpassword}@ds211613.mlab.com:11613/vue-ost`)
  .then(() => console.log('MongoDB connected'))
  .catch(error => console.error(error))

// API routes
require('./routes')(router)
app.use(router.routes())
app.use(router.allowedMethods())


app.listen(process.env.PORT, () => console.log(`Server is running on port ${process.env.PORT}`));
