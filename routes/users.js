const passport = require('koa-passport');
const Router = require('koa-router');
const router = new Router();

const usersController = require('../controllers/user')

router
  .get("/", passport.authenticate('jwt', { session: false }), usersController.fetch)
  .post("/add", passport.authenticate('jwt', { session: false }), usersController.add)
  .put("/:user_id/block", passport.authenticate('jwt', { session: false }), usersController.block);

module.exports = router.routes();
