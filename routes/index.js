module.exports = (router) => {
  //router.prefix('/v1')
  router.use('/datasets', require('./datasets'));
  router.use('/markups', require('./markups'));
  router.use('/auth', require('./auth'));
  router.use('/users', require('./users'));
}
