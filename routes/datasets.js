const passport = require('koa-passport');
const Router = require('koa-router');
const router = new Router();

const datasetController = require('../controllers/dataset')

router
  .get("/", passport.authenticate('jwt', { session: false }), datasetController.fetch)
  .post("/add", passport.authenticate('jwt', { session: false }), datasetController.add);

module.exports = router.routes();
