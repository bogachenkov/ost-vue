const passport = require('koa-passport');
const Router = require('koa-router');
const router = new Router();

const authController = require('../controllers/auth')

router
  .get("/user", passport.authenticate('jwt', { session: false }), authController.user)
  .post("/login", authController.login)
  .post("/register", authController.register);

module.exports = router.routes();
