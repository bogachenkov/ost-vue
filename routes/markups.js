const passport = require('koa-passport');
const Router = require('koa-router');
const router = new Router();

const markupsController = require('../controllers/markup')

router
  .get("/:folder", passport.authenticate('jwt', { session: false }), markupsController.fetch)
  .post("/add", passport.authenticate('jwt', { session: false }), markupsController.add);

module.exports = router.routes();
