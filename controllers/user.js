const bcrypt = require('bcryptjs');
const Joi = require('joi');

const User = require('../models/user');

// Validation schema
const schema = Joi.object().keys({
  login: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().regex(/^[a-zA-Z0-9]{6,30}$/).required(),
})

// Fetch all users
const fetch = async (ctx, next) => {
  const users = await User.find();
  ctx.body = users;
}

// Add a new user
const add = async (ctx, next) => {
  let { login, password } = ctx.request.body;

  // Validate data
  const {error, value} = Joi.validate({ login, password }, schema);
  if (error) ctx.throw(400, 'Проверьте правильность введенных данных');

  // Check if user already exists
  const user = await User.findOne({ login });
  if (user) ctx.throw(400, 'Логин уже занят');
  // Generate password's hash
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);
  const newUser = await new User({
    login,
    password: hash
  }).save()
  ctx.body = newUser;
}

// Toggle user's block
const block = async (ctx, next) => {
  const user = await User.findById(ctx.params.user_id);
  if (!user) ctx.throw(404, 'Пользователь не найден');
  user.blocked = !user.blocked;
  const updatedUser = await user.save();
  ctx.body = updatedUser;
}

module.exports = {
  fetch,
  add,
  block
}
