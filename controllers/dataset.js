const fs = require('fs');
const Joi = require('joi');
const Dataset = require('../models/dataset');

// Validation schema
const schema = Joi.object().keys({
  title: Joi.string().alphanum().required(),
  folder: Joi.string().token().required(),
})

const fetch = async (ctx, next) => {
  const datasets = await Dataset.find();
  ctx.body = datasets;
}

const add = async (ctx, next) => {

  const { folder, title } = ctx.request.body;

  // Validate data
  const {error, value} = Joi.validate({ title, folder }, schema);
  if (error) ctx.throw(400, 'Проверьте правильность введенных данных');

  // Check if dataset exists
  const datasetExists = await Dataset.findOne({ folder });
  if (datasetExists) ctx.throw(400, 'Датасет с указанной папкой уже существует');

  try {
    fs.accessSync(`datasets/${folder}`);
    const newDataset = await new Dataset({
      title,
      folder
    }).save();
    ctx.body = newDataset;
  } catch (err) {
    ctx.throw(404, 'Указанная папка не найдена');
  }
}

module.exports = {
  fetch,
  add
}
