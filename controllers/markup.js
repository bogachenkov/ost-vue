const fs = require('fs');

const Markup = require('../models/markup');
const User = require('../models/user');

// Get array of images, user hasn't marked
const fetch = async (ctx, next) => {
  console.log('fetch');
  //Get images folder from params
  const imagesFolder = ctx.params.folder;
  // Get array of filenames
  try {
    const files = fs.readdirSync(`datasets/${imagesFolder}`);
    const markups = await Markup.find({user: ctx.state.user.id, dataset: imagesFolder});
    if (!markups) ctx.body = files;
    const markedImagesArray = markups.map(item => item.image);
    const unmarkedImagesArray = files.filter(file => !markedImagesArray.includes(file));
    ctx.body = unmarkedImagesArray;
  } catch(e) {
    ctx.throw(500, 'Ошибка при работе с файлами');
  }
}

// Save coordinates of marked image
const add = async (ctx, next) => {
  const { body } = ctx.request;
  const markupData = {
    user: ctx.state.user.id,
    dataset: body.dataset,
    image: body.image,
    coords: body.coords
  }
  const markup = await new Markup(markupData).save();
  const user = await User.findById(ctx.state.user.id);
  user.imagesMarked++;
  await user.save();
  ctx.body = {
    name: markup.image
  }
}

module.exports = {
  fetch,
  add
}
