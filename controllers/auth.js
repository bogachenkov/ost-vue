require('dotenv').config()
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Joi = require('joi');

const User = require('../models/user');

// Validation schema
const schema = Joi.object().keys({
  login: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().regex(/^[a-zA-Z0-9]{6,30}$/).required(),
})

// Create jwt
const jwtSign = (user) => {
  return new Promise(( resolve, reject ) => {
    jwt.sign(
      {
        login:  user.login,
        id: user._id
      },
      process.env.SECRET_KEY,
      { expiresIn: '1h' },
      (error, token) => {
        if (error) reject(error);
        else {
          const data = {
            user: {
              id: user._id,
              login: user.login
            },
            token: `Bearer ${token}`
          };
          resolve(data)
        }
      }
    );
  })
}

// Login
const login = async (ctx, next) => {
  let { login, password } = ctx.request.body;

  // Validate data
  const {error, value} = Joi.validate({ login, password }, schema);
  if (error) ctx.throw(400, 'Проверьте правильность введенных данных');

  // Check if user exists
  const user = await User.findOne({ login }).select('+password');
  if (!user) ctx.throw(400,'Неверное сочетание логин/пароль');

  // Check password
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) ctx.throw(400, 'Неверное сочетание логин/пароль');

  // Sign jwt
  const result = await jwtSign(user);
  ctx.body = result;
}

// Register
const register = async (ctx, next) => {
  let { login, password } = ctx.request.body;

  // Validate data
  const {error, value} = Joi.validate({ login, password }, schema);
  if (error) ctx.throw(400, 'Проверьте правильность введенных данных');

// Check if user exists
  const user = await User.findOne({ login });
  if (user) ctx.throw(400, 'Логин уже занят');

  // Generate password's hash
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);
  const newUser = await new User({
    login,
    password: hash
  }).save()
  const result = await jwtSign(newUser);
  ctx.body = result;
}

// Get current user by token
const user = async (ctx, next) => {
  const user = await User.findById(ctx.state.user.id);
  if (!user) {
    ctx.status = 404;
    ctx.body = {
      message: 'Пользователь не найден'
    }
  }
  else ctx.body = {
    id: user.id,
    login: user.login
  }
}

module.exports = {
  login,
  register,
  user
}
